object Contourer {
  /**
   Determines the case number of the cell.
   */
  def eightCase(iso: Float, bl: Float, br: Float, tl: Float, tr: Float): Int = {
    val sixteenCase = (if (bl >= iso) 1 else 0) |
                      (if (br >= iso) 2 else 0) |
                      (if (tl >= iso) 4 else 0) |
                      (if (tr >= iso) 8 else 0)

    if (sixteenCase > 7) 15 - sixteenCase else sixteenCase
  }

  /**
   Bilinearly interpolate.
   */
  def lerp(x1: Float, y1: Float, x2: Float, y2: Float, x: Float): Float = {
    (x - x1) / (x2 - x1) * (y2 - y1) + y1
  }

  /**
   Determine where a contour line passes through this cell.
   */
  def cellToSegments(iso: Float)(c: Int, r: Int, bl: Float, br: Float, tl: Float, tr: Float): List[LineSegment] = {
    val configuration = eightCase(iso, bl, br, tl, tr)
    val intersections = new Array[Point2](4)

    // bottom edge
    if (configuration == 1 || configuration == 2 || configuration == 5 || configuration == 6) {
      intersections(0) = new Point2(lerp(bl, c, br, c + 1, iso), r)
    }

    // top edge
    if (configuration == 4 || configuration == 5 || configuration == 6 || configuration == 7) {
      intersections(1) = new Point2(lerp(tl, c, tr, c + 1, iso), r + 1)
    }

    // left edge
    if (configuration == 1 || configuration == 3 || configuration == 4 || configuration == 6) {
      intersections(2) = new Point2(c, lerp(bl, r, tl, r + 1, iso))
    }

    // right edge
    if (configuration == 2 || configuration == 3 || configuration == 6 || configuration == 7) {
      intersections(3) = new Point2(c + 1, lerp(br, r, tr, r + 1, iso))
    }
    
    configuration match {
      case 0 => List()
      case 1 => List(new LineSegment(intersections(0), intersections(2)))
      case 2 => List(new LineSegment(intersections(0), intersections(3)))
      case 3 => List(new LineSegment(intersections(2), intersections(3)))
      case 4 => List(new LineSegment(intersections(1), intersections(2)))
      case 5 => List(new LineSegment(intersections(0), intersections(1)))
      case 6 => List(new LineSegment(intersections(0), intersections(2)), new LineSegment(intersections(1), intersections(3)))
      case 7 => List(new LineSegment(intersections(1), intersections(3)))
    }
  }
}
