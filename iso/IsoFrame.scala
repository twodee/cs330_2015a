import javax.swing.JPanel
import java.awt.BasicStroke
import java.awt.Color
import java.awt.geom.Line2D
import java.awt.Graphics2D
import javax.swing.event.ChangeEvent
import javax.swing.event.ChangeListener
import java.awt.Graphics
import javax.swing.ImageIcon
import java.awt.BorderLayout
import javax.swing.JSlider
import javax.swing.JLabel
import javax.swing.JFrame

class IsoFrame(grid: FloatGrid) extends JFrame {
  val min = grid.min
  val max = grid.max
  private var segments : List[LineSegment] = List()

  val map = {
    val toolbar = new JPanel(new BorderLayout)
    val minLabel = new JLabel("" + min)
    val maxLabel = new JLabel("" + max)
    val isoSlider = new JSlider(0, 1000)

    toolbar.add(minLabel, BorderLayout.WEST)
    toolbar.add(isoSlider, BorderLayout.CENTER)
    toolbar.add(maxLabel, BorderLayout.EAST)
    add(toolbar, BorderLayout.NORTH)

    val map = grid.toImage
    
    val isoPanel = new IsoPanel
    add(isoPanel, BorderLayout.CENTER)

    isoSlider.addChangeListener(new ChangeListener {
      def stateChanged(e: ChangeEvent) {
        if (!isoSlider.getValueIsAdjusting()) {
          val range : Float = isoSlider.getMaximum - isoSlider.getMinimum
          val proportion = (isoSlider.getValue() - isoSlider.getMinimum) / range
          val iso = proportion * (max - min) + min

          segments = grid.mapOverCells(Contourer.cellToSegments(iso)).flatten
          repaint()
        }
      }
    })

    map
  }

  setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  setSize(512, 512);
  setVisible(true);

  class IsoPanel extends JPanel {
    override def paintComponent(g: Graphics) {
      val g2 = g.asInstanceOf[Graphics2D]
      val oldTransform = g2.getTransform

      val gridAspect = grid.width / grid.height.toDouble
      val windowAspect = getWidth / getHeight.toDouble
      val scaleFactor = if (windowAspect < gridAspect) {
        getWidth / grid.width.toDouble
      } else {
        getHeight / grid.height.toDouble
      }
      g2.scale(scaleFactor, scaleFactor)
      g2.drawImage(map, 0, 0, null)

      g2.setStroke(new BasicStroke((1.0 / scaleFactor).toFloat))

      g2.translate(0.5, 0.5)
      g2.setColor(Color.YELLOW)
      segments.foreach {segment => {
        g2.draw(new Line2D.Float(segment.a.x, segment.a.y, segment.b.x, segment.b.y))
      }}

      g2.setTransform(oldTransform)
    }
  }
}

object IsoFrame {
  def main(args: Array[String]) {
    val grid = new FloatGrid(args(0))
    new IsoFrame(grid)
  }
}
