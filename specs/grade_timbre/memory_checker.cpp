#include <iostream>

#include "Music.h"
#include "Samples.h"

int main(int argc, char **argv) {
  float *data1 = new float[1000];
  for (int i = 0; i < 1000; ++i) {
    data1[i] = 2.0f * rand() / (float) RAND_MAX - 1.0f;
  }
  Samples s1(1000, data1);
  s1.WriteWAV(argv[1]);

  float *data2 = new float[3];
  data2[0] = data2[1] = data2[2] = 5;
  Samples s2(3, data2);

  float *data3 = new float[3];
  data3[0] = data3[1] = data3[2] = 25;
  Samples s3(3, data3);

  s2 |= s3;

  Samples s4 = s1;
  s4 = s1 * 5;
  s4 *= 5;
  s1 += s4;
  s1 += s2;

  s1 = s1;
  
  return 0;
}
