import org.scalatest._
import java.awt.Color

object SpecChecker extends FunSuite {
  val Epsilon = 1.0e-3f
}

class SpecChecker extends FunSuite with BeforeAndAfter with Matchers {

  var p43 : Point2 = _
  var p0n8 : Point2 = _
  var p07314 : Point2 = _
  var l011011 : LineSegment = _
  var grid : FloatGrid = _

  before {
    p43 = new Point2(4, 3)
    p0n8 = new Point2(0, -8)
    p07314 = new Point2(0.7f, 3.14f)
    l011011 = new LineSegment(new Point2(0, 1), new Point2(10, 11))
    grid = new FloatGrid(System.getProperty("org.twodee.graderdir") + "/sub.3.2.bin")
  }

  // 4, 3 tests
  test("Does Point2(4, 3).x yield the expected x-coordinate?") {
    p43.x should be (4.0f +- SpecChecker.Epsilon)
  }

  test("Does Point2(4, 3).y yield the expected y-coordinate?") {
    p43.y should be (3.0f +- SpecChecker.Epsilon)
  }

  test("Does Point2(4, 3).toString yield the expected String?") {
    p43.toString should equal ("4.0 3.0")
  }

  // 0, -8 tests
  test("Does Point2(0, -8).x yield the expected x-coordinate?") {
    p0n8.x should be (0.0f +- SpecChecker.Epsilon)
  }

  test("Does Point2(0, -8).y yield the expected y-coordinate?") {
    p0n8.y should be (-8.0f +- SpecChecker.Epsilon)
  }

  test("Does Point2(0, -8).toString yield the expected String?") {
    p0n8.toString should equal ("0.0 -8.0")
  }

  // 0.7, 3.14 tests
  test("Does Point2(0.7, 3.14).x yield the expected x-coordinate?") {
    p07314.x should be (0.7f +- SpecChecker.Epsilon)
  }

  test("Does Point2(0.7, 3.14).y yield the expected y-coordinate?") {
    p07314.y should be (3.14f +- SpecChecker.Epsilon)
  }

  test("Does Point2(0.7, 3.14).toString yield the expected String?") {
    p07314.toString should equal ("0.7 3.14")
  }

  test("Does LineSegment((0, 1), (10, 11)) yield the expected first endpoint?") {
    l011011.a.x should be (0.0f +- SpecChecker.Epsilon)
    l011011.a.y should be (1.0f +- SpecChecker.Epsilon)
  }

  test("Does LineSegment((0, 1), (10, 11)) yield the expected second endpoint?") {
    l011011.b.x should be (10.0f +- SpecChecker.Epsilon)
    l011011.b.y should be (11.0f +- SpecChecker.Epsilon)
  }

  test("Does LineSegment((0, 1), (10, 11)).toString yield the expected String?") {
    l011011.toString should equal ("0.0 1.0 to 10.0 11.0")
  }

  // Mutability tests
  test("Does Point2 use immutable val instance variables?") {
    if (classOf[Point2].getMethods.map(method => method.getName).exists(name => name == "x_$eq")) {
      fail("Point2 class has an x_$eq method.")
    }
    if (classOf[Point2].getMethods.map(method => method.getName).exists(name => name == "y_$eq")) {
      fail("Point2 class has a y_$eq method.")
    }
  }

  test("Does LineSegment use immutable val instance variables?") {
    if (classOf[LineSegment].getMethods.map(method => method.getName).exists(name => name == "a_$eq")) {
      fail("LineSegment class has an a_$eq method.")
    }
    if (classOf[LineSegment].getMethods.map(method => method.getName).exists(name => name == "b_$eq")) {
      fail("LineSegment class has a b_$eq method.")
    }
  }

  // FloatGrid
  test("Does FloatGrid.min yield expected value for sub.3.2.bin?") {
    grid.min should be (317.81967f +- SpecChecker.Epsilon)
  }

  test("Does FloatGrid.max yield expected value for sub.3.2.bin?") {
    grid.max should be (320.62427f +- SpecChecker.Epsilon)
  }

  test("Does FloatGrid.width yield expected value for sub.3.2.bin?") {
    grid.width should be (3)
  }

  test("Does FloatGrid.height yield expected value for sub.3.2.bin?") {
    grid.height should be (2)
  }

  test("Does FloatGrid.apply yield the expected values for sub.3.2.bin?") {
    withClue ("grid(0, 0): ") {
      grid(0, 0) should be (320.624268f +- SpecChecker.Epsilon)
    }
    withClue ("grid(1, 0): ") {
      grid(1, 0) should be (319.010956f +- SpecChecker.Epsilon)
    }
    withClue ("grid(2, 1): ") {
      grid(2, 1) should be (317.960236f +- SpecChecker.Epsilon)
    }
    withClue ("grid(0, 1): ") {
      grid(0, 1) should be (318.697723f +- SpecChecker.Epsilon)
    }
    withClue ("grid(1, 1): ") {
      grid(1, 1) should be (317.919037f +- SpecChecker.Epsilon)
    }
    withClue ("grid(2, 0): ") {
      grid(2, 0) should be (317.819672f +- SpecChecker.Epsilon)
    }
  }

  test("Does FloatGrid.each on sub.3.2.bin visit and process each sample, where process means sum?") {
    var sum : Float = 0.0f
    grid.each((c, r, intensity) => sum += intensity);
    sum should be (1912.0319f +- SpecChecker.Epsilon);
  }

  test("Does FloatGrid.each on sub.3.2.bin visit and process each sample, where process means find the maximum?") {
    var max : Float = Float.MinValue
    grid.each((c, r, intensity) => max = if (intensity > max) intensity else max);
    max should be (320.624268f +- SpecChecker.Epsilon);
  }

  test("Does FloatGrid.toImage on sub.3.2.bin yield the expected image?") {
    val min = grid.min
    val max = grid.max
    val range = max - min
    val image = grid.toImage
    image.getWidth should be (3)
    image.getHeight should be (2)
    for (r <- 0 until image.getHeight; c <- 0 until image.getWidth) {
      val color = new Color(image.getRGB(c, r))
      val scaled = (255.0f * (grid(c, r) - min) / (max - min)).toInt
      withClue ("image(" + c + "," + r + ").getRed: ") {
        color.getRed should be (scaled +- 1)
      }
      withClue ("image(" + c + "," + r + ").getGreen: ") {
        color.getGreen should be (scaled +- 1)
      }
      withClue ("image(" + c + "," + r + ").getBlue: ") {
        color.getBlue should be (scaled +- 1)
      }
    }
  }

  test("Does FloatGrid.mapOverCells on sub.3.2.bin process each 2x2 neighborhood, where process means yield the bottom-left elevation?") {
    val intensities = grid.mapOverCells((c, r, bl, br, tl, tr) => {
      bl
    })
    intensities.zip(List(320.62427f, 319.01096f)).foreach {case (actual, expected) => {
      actual should be (expected +- SpecChecker.Epsilon)
    }}
  }
}
